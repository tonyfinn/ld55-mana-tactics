extends Object

const TILE_GRASSLAND = 'grassland'
const TILE_LAKE = 'lake'
const TILE_VOLCANO = 'volcano'
const TILE_MOUNTAIN = 'mountain'
const TILE_SPRING = 'spring'
const TILE_FOREST = 'forest'
const TILE_NAMES = {
	TILE_GRASSLAND: "Grassland",
	TILE_LAKE: "Lake",
	TILE_VOLCANO: "Volcano",
	TILE_MOUNTAIN: "Mountain",
	TILE_SPRING: "Spring",
	TILE_FOREST: "Forest"
}

const TILE_OWNER_NONE = 'none'
const PLAYER_P1 = 'player_1';
const PLAYER_P2 = 'player_2';

const TILE_HIGHLIGHT_ACTIONABLE = 'actionable'
const TILE_HIGHLIGHT_NONE = 'default'
const TILE_HIGHLIGHT_MOVEMENT = 'movement'
const TILE_HIGHLIGHT_SELECTED = 'selected'

const UI_MODE_NORMAL = 'normal'
const UI_MODE_MOVING = 'moving'
const UI_MODE_SUMMONING = 'summoning'
const UI_MODE_ATTACKING = 'attacking'
const UI_MODE_INFO = 'info'

const ACTION_MOVE = 'move'
const ACTION_SUMMON = 'summon'
const ACTION_ATTACK = 'attack'

const UNIT_TYPE_SUMMONER = 'summoner'
const UNIT_TYPE_FIRE = 'fire_spirit'
const UNIT_TYPE_WATER = 'water_poseidon'
const UNIT_TYPE_EARTH = 'earth_golem'
const UNIT_TYPE_LIFE = 'living_tree'
const SUMMONABLE_UNITS: Array[String] = [UNIT_TYPE_FIRE, UNIT_TYPE_WATER, UNIT_TYPE_EARTH, UNIT_TYPE_LIFE]

const RESOURCE_LIFE = 'life'
const RESOURCE_WATER = 'water'
const RESOURCE_FIRE = 'fire'
const RESOURCE_EARTH = 'earth'
const RESOURCE_NAMES = {
	RESOURCE_LIFE: "Life",
	RESOURCE_WATER: "Water",
	RESOURCE_FIRE: "Fire",
	RESOURCE_EARTH: "Earth"
}

const ALL_RESOURCES = [RESOURCE_LIFE, RESOURCE_WATER, RESOURCE_FIRE, RESOURCE_EARTH]


const UATTR_DISPLAY_NAME = 'display_name'
const UATTR_ANIMATION = 'animation'
const UATTR_ATTACK = 'attack'
const UATTR_HP = 'hit_points'
const UATTR_MOVEMENT_SPEED = 'movement_speed'
const UATTR_SUMMONABLE = 'summonable'
const UATTR_COSTS = 'summon_costs'
const UATTR_TYPE_NAME = 'type_name'
const UATTR_ATTACK_RANGE = 'attack_range'

const UNIT_STATS = {
	Constants.UNIT_TYPE_SUMMONER: {
		UATTR_TYPE_NAME: Constants.UNIT_TYPE_SUMMONER,
		UATTR_DISPLAY_NAME: 'Summoner',
		UATTR_ANIMATION: 'summoner',
		UATTR_ATTACK: 0,
		UATTR_ATTACK_RANGE: 1,
		UATTR_HP: 10,
		UATTR_MOVEMENT_SPEED: 2,
		UATTR_SUMMONABLE: false,
		UATTR_COSTS: []
	},
	Constants.UNIT_TYPE_EARTH: {
		UATTR_TYPE_NAME: Constants.UNIT_TYPE_EARTH,
		UATTR_DISPLAY_NAME: 'Golem',
		UATTR_ANIMATION: 'golem',
		UATTR_ATTACK: 2,
		UATTR_ATTACK_RANGE: 1,
		UATTR_HP: 10,
		UATTR_MOVEMENT_SPEED: 2,
		UATTR_SUMMONABLE: true,
		UATTR_COSTS: [Constants.RESOURCE_EARTH, Constants.RESOURCE_EARTH, Constants.RESOURCE_EARTH]
	},
	Constants.UNIT_TYPE_FIRE: {
		UATTR_TYPE_NAME: Constants.UNIT_TYPE_FIRE,
		UATTR_DISPLAY_NAME: 'Fire Spirit',
		UATTR_ANIMATION: 'flame_spirit',
		UATTR_ATTACK: 4,
		UATTR_ATTACK_RANGE: 2,
		UATTR_HP: 2,
		UATTR_MOVEMENT_SPEED: 3,
		UATTR_SUMMONABLE: true,
		UATTR_COSTS: [Constants.RESOURCE_FIRE, Constants.RESOURCE_FIRE, Constants.RESOURCE_FIRE]
	},
	Constants.UNIT_TYPE_WATER: {
		UATTR_TYPE_NAME: Constants.UNIT_TYPE_WATER,
		UATTR_DISPLAY_NAME: 'Poseidon',
		UATTR_ANIMATION: 'poseidon',
		UATTR_ATTACK: 1,
		UATTR_ATTACK_RANGE: 1,
		UATTR_HP: 5,
		UATTR_MOVEMENT_SPEED: 4,
		UATTR_SUMMONABLE: true,
		UATTR_COSTS: [Constants.RESOURCE_WATER, Constants.RESOURCE_WATER, Constants.RESOURCE_WATER]
	},
	Constants.UNIT_TYPE_LIFE: {
		UATTR_TYPE_NAME: Constants.UNIT_TYPE_LIFE,
		UATTR_DISPLAY_NAME: 'Living Tree',
		UATTR_ANIMATION: 'living_tree',
		UATTR_ATTACK: 3,
		UATTR_ATTACK_RANGE: 1,
		UATTR_HP: 5,
		UATTR_MOVEMENT_SPEED: 3,
		UATTR_SUMMONABLE: true,
		UATTR_COSTS: [Constants.RESOURCE_LIFE, Constants.RESOURCE_LIFE, Constants.RESOURCE_LIFE]
	}
}
