extends Node2D

@export var width = 32
@export var height = 18
@export var map_tile_scene: PackedScene
@export var unit_scene: PackedScene

var map_tile_width = 40
var map_tile_height = 40

var units: Array[Unit] = []
var active_unit: Unit
var tiles = []
var actionable_tiles: Array[MapTile] = []
var flood_fill: FloodFill
var move_options = null
var attack_options = null


var players: Array[Player] = []
var turn_count = 1
var current_player_turn = 0
var current_player: Player:
	get:
		return players[current_player_turn]
	set(value):
		current_player_turn = value.player_id
		
var opposing_player: Player:
	get:
		if current_player_turn == 1:
			return players[0]
		else: 
			return players[1]
		
var summoning_unit = null
var game_active = false


var mode = Constants.UI_MODE_NORMAL

const LAKES = [
	Vector2i(2, 4),
	Vector2i(3, 3),
	Vector2i(3, 4),
	Vector2i(19, 9), 
	Vector2i(21, 2), 
	Vector2i(17, 3), 
	Vector2i(32, 16), 
	Vector2i(17, 9), 
	Vector2i(12, 17), 
	Vector2i(8, 7), 
	Vector2i(12, 3), 
	Vector2i(5, 11), 
	Vector2i(13, 11)
];

const VOLCANOES = [
	Vector2i(20, 9),
	Vector2i(2, 6),
	Vector2i(7, 13),
	Vector2i(13, 2),
	Vector2i(6, 14),
	Vector2i(32, 13),
	Vector2i(11, 14),
	Vector2i(14, 5),
	Vector2i(21, 11),
	Vector2i(4, 17)
]

const MOUNTAINS = [
	Vector2i(19, 2),
	Vector2i(6, 15),
	Vector2i(2, 6),
	Vector2i(2, 16),
	Vector2i(7, 15),
	Vector2i(25, 3),
	Vector2i(7, 6),
	Vector2i(30, 13),
	Vector2i(9, 13),
	Vector2i(14, 5)
];

const SPRINGS = [
	Vector2i(30, 15),
	Vector2i(23, 6),
	Vector2i(1, 17),
	Vector2i(1, 10),
	Vector2i(0, 4),
	Vector2i(21, 16),
	Vector2i(21, 9),
	Vector2i(4, 8),
	Vector2i(9, 11),
	Vector2i(11, 13)
];

const FORESTS = [
	Vector2i(10, 8),
	Vector2i(20, 7),
	Vector2i(10, 4),
	Vector2i(6, 10),
	Vector2i(28, 13),
	Vector2i(15, 7),
	Vector2i(12, 3),
	Vector2i(18, 10),
	Vector2i(22, 14),
	Vector2i(8, 8)
];

func _on_tile_clicked(tile: MapTile):
	if not game_active:
		return
	print("Clicked on tile (", tile.x, ", ", tile.y, ")")
	print("Tile unit", tile.current_unit)
	print('Actionable tiles: ', actionable_tiles)
	if self.mode == Constants.UI_MODE_NORMAL:
		if self.active_unit != null:
			deactivate_unit()
		if tile.current_unit:
			unit_clicked(tile.current_unit)
		else:
			$UI.hide_toolbar()
	elif self.mode == Constants.UI_MODE_INFO:
		$UI.show_tile_info(tile)
		if tile.current_unit:
			$UI.show_unit_info(tile.current_unit)
	elif self.mode != Constants.UI_MODE_NORMAL and not tile in actionable_tiles:
		print('Clearing tile actions as clicked on irrelevant tile')
		if self.mode == Constants.UI_MODE_MOVING:
			$UI.show_popup_text(tile.position, "Cannot move there")
		if self.mode == Constants.UI_MODE_ATTACKING:
			$UI.show_popup_text(tile.position, "Cannot attack there")
		if self.mode == Constants.UI_MODE_SUMMONING:
			$UI.show_popup_text(tile.position, "Cannot summon there")
		$UI.hide_toolbar()
		if self.active_unit != null:
			deactivate_unit()
		self.mode = Constants.UI_MODE_NORMAL
		move_options = null
		attack_options = null
		clear_actionable_tiles()
	elif mode == Constants.UI_MODE_SUMMONING and tile.current_unit == null:
		do_summon_action(summoning_unit, tile)
	elif (mode == Constants.UI_MODE_MOVING
		and active_unit != null 
		and move_options != null and tile.coords() in move_options):
		do_move_action(active_unit, tile)
	elif (mode == Constants.UI_MODE_ATTACKING
		and active_unit != null
		and attack_options != null and tile.coords() in attack_options):
		do_attack_action(active_unit, tile)


func do_summon_action(summon_unit_type: String, target: MapTile):
	if summon_unit_type != null:
		var unit_data = Constants.UNIT_STATS[summon_unit_type]
		var costs = unit_data[Constants.UATTR_COSTS]
		if current_player.can_afford(costs):
			print('Summoning a unit at (', target.x, ", ", target.y, ")")
			spawn_unit(summon_unit_type, current_player, target.grid_pos)
			summoning_unit = null
			current_player.deduct_costs(costs)
			active_unit.times_summoned += 1
			$UI.update_player_resources(current_player)
			$UI.show_popup_text(current_player.units[0].position, 
				str(current_player.units[0].times_summoned) + " / 2")
			$UI.show_popup_text(target.position, 'Summoned ' + unit_data[Constants.UATTR_DISPLAY_NAME])
		else:
			$UI.show_popup_text(target.position, 'Not enough resources to summon')
	finish_action()

func do_attack_action(attacker: Unit, target_tile: MapTile):
	deactivate_unit()
	if target_tile.current_unit == null:
		$UI.show_popup_text(target_tile.position, "No Unit To Attack")
	elif target_tile.current_unit.owned_by == self.current_player:
		$UI.show_popup_text(target_tile.position, "Can't attack friendly unit")
	else:
		attacker.attack(target_tile.current_unit)
	finish_action()

func do_move_action(unit: Unit, target: MapTile):
	unit.current_tile.set_highlight(Constants.TILE_HIGHLIGHT_NONE)
	unit.move_to(target)
	if target.is_ownable():
		current_player.capture_tile(target)
		$UI.update_player_resources(current_player)
	finish_action()
	
func finish_action():
	deactivate_unit()
	clear_actionable_tiles()
	self.mode = Constants.UI_MODE_NORMAL
	$UI.hide_toolbar()
		
func get_tile(coord: Vector2i) -> MapTile:
	return tiles[coord.x][coord.y]
	
func deactivate_unit():
	if self.active_unit:
		var tile = get_tile(self.active_unit.coords())
		tile.set_highlight(Constants.TILE_HIGHLIGHT_NONE)
		self.active_unit = null
		$UI.hide_unit_info()
	
func unit_clicked(unit):
	if not game_active:
		return
	deactivate_unit()
	print("Unit clicked: ", unit)
	self.active_unit = unit
	var active_tile = get_tile(self.active_unit.coords())
	active_tile.set_highlight(Constants.TILE_HIGHLIGHT_SELECTED)
	
	if active_unit.owned_by == current_player:
		var actions: Array[String] = []
		if unit.can_move():
			actions.push_back(Constants.ACTION_MOVE)
		if unit.can_summon():
			actions.push_back(Constants.ACTION_SUMMON)
		if unit.can_attack():
			actions.push_back(Constants.ACTION_ATTACK)
		$UI.show_toolbar(actions)
		

func indicate_summon_locations():
	var unit_tile = self.active_unit.current_tile
	var x = unit_tile.x
	var y = unit_tile.y
	mode = Constants.UI_MODE_SUMMONING
	if y > 0 and is_summonable_tile(x, y-1):
		indicate_tile_actionable(x, y-1)
	if x > 0 and is_summonable_tile(x-1, y):
		indicate_tile_actionable(x-1, y)
	if x < (width-1) and is_summonable_tile(x+1, y):
		indicate_tile_actionable(x+1, y)
	if y < (height-1) and is_summonable_tile(x, y+1):
		indicate_tile_actionable(x, y+1)	
		
func _on_ui_summon_clicked(unit_type_name):
	self.summoning_unit = unit_type_name
	indicate_summon_locations()
		
func _on_action_clicked(action: String):
	clear_actionable_tiles()
	if action == Constants.ACTION_SUMMON:
		var affordable_units: Array[String] = []
		var unaffordable_units: Array[String] = []
		for summon_unit_type in Constants.SUMMONABLE_UNITS:
			var unit_data = Constants.UNIT_STATS[summon_unit_type]
			var costs = unit_data[Constants.UATTR_COSTS]
			if current_player.can_afford(costs):
				affordable_units.push_back(summon_unit_type)
			else:
				unaffordable_units.push_back(summon_unit_type)
		$UI.show_summon_toolbar(affordable_units, unaffordable_units)
	if self.active_unit != null and action == Constants.ACTION_MOVE:
		var unit_tile = self.active_unit.current_tile
		move_options = flood_fill.find_routes(unit_tile.coords(), self.active_unit.movement_speed, true, false)
		for point in move_options.keys():
			indicate_tile_actionable(point.x, point.y)
		self.mode = Constants.UI_MODE_MOVING
		$UI.hide_toolbar()
	if action == Constants.ACTION_ATTACK:
		var unit_tile = self.active_unit.current_tile
		attack_options = flood_fill.find_routes(unit_tile.coords(), self.active_unit.attack_range, false, true)
		for point in attack_options.keys():
			indicate_tile_actionable(point.x, point.y)
		self.mode = Constants.UI_MODE_ATTACKING
		$UI.hide_toolbar()
			
	print(action)
	
func is_summonable_tile(x, y):
	return tiles[x][y].current_unit == null and tiles[x][y].terrain_type != Constants.TILE_LAKE
	
func indicate_tile_actionable(x, y):
	var tile = tiles[x][y]
	actionable_tiles.push_back(tile)
	tile.set_highlight(Constants.TILE_HIGHLIGHT_ACTIONABLE)
	
func clear_actionable_tiles():
	for tile in actionable_tiles:
		tile.set_highlight(Constants.TILE_HIGHLIGHT_NONE)
	actionable_tiles = []
	
func mtile(v: Vector2i) -> MapTile:
	return tiles[v.x][v.y]
	
func spawn_unit(unit_type: String, player: Player, coords: Vector2i):
	var unit = unit_scene.instantiate()
	unit.grid_pos = coords
	unit.set_owned_by(player)
	unit.set_type(unit_type)
	unit.internal_move_to(mtile(coords))
	unit.connect("unit_injured", _on_unit_injured)
	units.push_back(unit)
	player.units.push_back(unit)
	add_child(unit)
	return unit
	
func _on_unit_injured(unit, damage, remaining_hp):
	print("Damaged unit", unit)
	if remaining_hp > 0:
		$UI.show_popup_text(
			unit.position,
			"Unit damaged: " + str(damage) + "HP \n"
			+ "(" + str(unit.hp) + "/" + str(unit.max_hp) + " remaining)"
		)
	else:
		$UI.show_popup_text(unit.position, "Unit defeated")
		var all_units_idx = units.find(unit)
		units.remove_at(all_units_idx)
		var player_units_idx = unit.owned_by.units.find(unit)
		unit.owned_by.units.remove_at(player_units_idx)
		unit.remove()
		if unit.unit_type == Constants.UNIT_TYPE_SUMMONER:
			game_active = false
			$UI.game_over(current_player)


func generate_map():
	tiles = []
	for x in range(width):
		tiles.push_back([])
		for y in range(height):
			var tile = map_tile_scene.instantiate()
			tile.x = x
			tile.y = y
			var rng = randi_range(0, 100)
			if ((tile.x == 5 and tile.y == 5) or
				(tile.x == 27 and tile.y == 13)):
				tile.set_type(Constants.TILE_GRASSLAND)
			elif rng < 10:
				tile.set_type(Constants.TILE_LAKE)
			elif rng < 13:
				tile.set_type(Constants.TILE_MOUNTAIN)
			elif rng < 16:
				tile.set_type(Constants.TILE_VOLCANO)
			elif rng < 19:
				tile.set_type(Constants.TILE_SPRING)
			elif rng < 22:
				tile.set_type(Constants.TILE_FOREST)
			else:
				tile.set_type(Constants.TILE_GRASSLAND)
			tile.position = tile.screen_pos()
			tiles[x].push_back(tile)
			add_child(tile)
			tile.connect("tile_clicked", _on_tile_clicked)

func start_game():
	for unit in units:
		unit.queue_free()
	units = []
	for row in tiles:
		for tile in row:
			tile.queue_free()
	current_player_turn = 0
	turn_count = 1
	active_unit = null
	summoning_unit = null
	mode = Constants.UI_MODE_NORMAL
	players = [Player.new(0), Player.new(1)]
	actionable_tiles = []
	move_options = []
	attack_options = []
	
	generate_map()
	
	spawn_unit(Constants.UNIT_TYPE_SUMMONER, players[0], Vector2i(5, 5))
	spawn_unit(Constants.UNIT_TYPE_SUMMONER, players[1], Vector2i(27, 13))
	
	flood_fill = FloodFill.new(tiles)
	$UI.set_turn(1)
	$UI.set_player(current_player)
	current_player.start_turn()
	$UI.update_player_resources(current_player)
	for unit in opposing_player.units:
		unit.dim()
	

# Called when the node enters the scene tree for the first time.
func _ready():
	start_game()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_end_turn():
	current_player.end_turn()
	$UI.show_popup_text(current_player.units[0].position, "Player " + str(current_player.player_id + 1) + ": End Turn")
	deactivate_unit()
	mode = Constants.UI_MODE_NORMAL
	$UI.hide_toolbar()
	current_player_turn = (current_player_turn + 1) % players.size()
	if current_player_turn == 0:
		turn_count += 1
	$UI.set_turn(turn_count)
	$UI.set_player(current_player)
	current_player.start_turn()
	$UI.show_popup_text(current_player.units[0].position, "Player " + str(current_player.player_id + 1) + ": Start Turn")
	$UI.update_player_resources(current_player)
	for unit in opposing_player.units:
		unit.dim()


func _on_ui_game_started():
	game_active = true
	$UI.show_popup_text(current_player.units[0].position, "Player " + str(current_player.player_id + 1) + ": Start Turn")
	current_player.start_turn()
	$UI.update_player_resources(current_player)


func _on_ui_info_clicked():
	if mode != Constants.UI_MODE_INFO:
		finish_action()
		mode = Constants.UI_MODE_INFO
	else:
		finish_action()
		$UI.clear_info_mode()


func _on_ui_game_restarted():
	start_game()
	game_active = true
