# Ludum 55

## Contest theme - summoning

- Magic based summoning
- Gameplay - Tactics RPG style battles
- Summoning resources are going to be capturable locations on the battle map
  - a bit like cities in advance wars
    - I'm thinking each capture point gives one of its relevant resource per turn
    - Volcano gives fire element points
    - Lake gives water element points
    - Farm gives life element points
    - Mountain gives earth element points
    - etc.
- Combine to do summons
  - Water + Life = Poseidon/Mermaid thing
  - Fire + Earth = Some sort of lava monster
  - etc. 
- Use extra resources to boost your creatures at summon time
- Some sort of cool magic circle summonining animation that uses your elements in its presentation
- Maybe turn based multiplayer


### Battle Map

- Map is going have a bunch of tiles
- Size: 32x18
    - 30 resource structures
    - 576 tiles
    - + player HQ per player
- Each tile has a terrain type
- Mostly plain grassland


####