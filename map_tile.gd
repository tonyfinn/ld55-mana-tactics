extends Node2D

class_name MapTile

signal tile_clicked(tile: MapTile)
signal tile_hovered(tile: MapTile)
signal tile_exited(tile: MapTile)

@export var terrain_type: String
@export var x: int
@export var y: int
@export var mouse_over: bool
@export var highlight_state: String = 'inactive'

var owned_by: Player = null

var current_unit: Unit = null

const MOVEMENT_COST = 'movement_cost'
const OUTPUT_RESOURCES = 'output_resources'

static var TILE_DATA = {
	Constants.TILE_VOLCANO: {
		MOVEMENT_COST: 1,
		OUTPUT_RESOURCES: [Constants.RESOURCE_FIRE]
	},
	Constants.TILE_SPRING: {
		MOVEMENT_COST: 1,
		OUTPUT_RESOURCES: [Constants.RESOURCE_WATER]
	},
	Constants.TILE_MOUNTAIN: {
		MOVEMENT_COST: 2,
		OUTPUT_RESOURCES: [Constants.RESOURCE_EARTH]
	},
	Constants.TILE_FOREST: {
		MOVEMENT_COST: 1,
		OUTPUT_RESOURCES: [Constants.RESOURCE_LIFE]
	},
	Constants.TILE_LAKE: {
		MOVEMENT_COST: 100,
		OUTPUT_RESOURCES: []
	},
	Constants.TILE_GRASSLAND: {
		MOVEMENT_COST: 1,
		OUTPUT_RESOURCES: []
	}
}

var grid_pos: Vector2i:
	get:
		return Vector2i(self.x, self.y)
	set(value):
		self.x = value.x
		self.y = value.y
		
func screen_pos() -> Vector2:
	return Vector2(
		(self.x * 40) + 20, 
		(self.y * 40) + 20
	)	

func is_ownable():
	return self.terrain_type in [Constants.TILE_VOLCANO, Constants.TILE_SPRING, Constants.TILE_FOREST, Constants.TILE_MOUNTAIN]

func is_in_self(position):
	var tile_tl_x = self.position.x - 20
	var tile_tl_y = self.position.y - 20
	return (position.x >= tile_tl_x and
		position.y >= tile_tl_y and 
		position.y < tile_tl_y + 40 and
		position.x < tile_tl_x + 40)
		
func movement_cost():
	return TILE_DATA[self.terrain_type][MOVEMENT_COST]
	
func output_resources():
	return TILE_DATA[self.terrain_type][OUTPUT_RESOURCES]
		
func is_passable():
	return current_unit == null


func _unhandled_input(event):
	if event is InputEventMouseMotion:
		if is_in_self(event.position):
			if not mouse_over:
				$MouseoverHighlight.show()
				tile_hovered.emit(self)
				mouse_over = true
		else:
			$MouseoverHighlight.hide()
			if mouse_over:
				mouse_over = false
				tile_exited.emit(self)
	elif event is InputEventMouseButton:
		if event.pressed and is_in_self(event.position):
			tile_clicked.emit(self)


# Called when the node enters the scene tree for the first time.
func _ready():
	mouse_over = false
	$MouseoverHighlight.hide()
	$ActiveHighlight.hide()
	$TileSprite.animation = self.terrain_type
	$OwnerFlag.hide()
	
	
func set_type(type: String):
	self.terrain_type = type
	$TileSprite.animation = type
	
	
func set_highlight(highlight_type: String):
	highlight_state = highlight_type
	$ActiveHighlight.animation = highlight_type
	if highlight_type == Constants.TILE_HIGHLIGHT_NONE:
		$ActiveHighlight.hide()
	else:
		if highlight_type == Constants.TILE_HIGHLIGHT_SELECTED:
			$ActiveHighlight.play()
		$ActiveHighlight.show()
func coords() -> Vector2i:
	return Vector2i(self.x, self.y)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _to_string():
	return '<MapTile(' + str(self.x) + ", " + str(self.y) + ")>"

func set_owned_by(owner):
	self.owned_by = owner
	if owner != null:
		$OwnerFlag.self_modulate = owner.tint_color
		$OwnerFlag.show()
	else:
		$OwnerFlag.hide()
