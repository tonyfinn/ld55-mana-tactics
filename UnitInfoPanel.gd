extends PanelContainer



func show_for_unit(unit: Unit):
	$MarginContainer/VBoxContainer/UnitNameLabel.text = unit.label
	$MarginContainer/VBoxContainer/GridContainer/MovementSpeedAmount.text = str(unit.movement_speed)
	$MarginContainer/VBoxContainer/GridContainer/AttackPowerAmount.text = str(unit.attack_power)
	$MarginContainer/VBoxContainer/GridContainer/AttackRangeAmount.text = str(unit.attack_range)
	var unit_hp_text = str(unit.hp) + " / " + str(unit.max_hp)
	$MarginContainer/VBoxContainer/GridContainer/HPAmount.text = unit_hp_text
	$MarginContainer/VBoxContainer/GridContainer/PositionValue.text = "(" + str(unit.grid_pos.x + 1) + ", " + str(unit.grid_pos.y + 1) + ")"
	var owned_text = "None"
	if unit.owned_by != null:
		owned_text = "Player " + str(unit.owned_by.player_id + 1)
	$MarginContainer/VBoxContainer/GridContainer/OwnerValue.text = owned_text
	var preview_unit = $MarginContainer/VBoxContainer/PanelContainer/Spacer/Unit
	preview_unit.set_owned_by(unit.owned_by)
	preview_unit.set_type(unit.unit_type)
	

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
