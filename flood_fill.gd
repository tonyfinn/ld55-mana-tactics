extends Object

class_name FloodFill

var map_data
var width: int
var height: int

class Search:
	var destinations = {}
	var search_points = {}
	var map_data = {}
	var width
	var height
	var consider_passability
	var uniform_costs
	
	func _init(map_data, consider_passability, uniform_costs):
		self.map_data = map_data
		self.width = map_data.size()
		self.height = map_data[0].size()
		self.consider_passability = consider_passability
		self.uniform_costs = uniform_costs
		
	func get_neighbouring_tiles(point: Vector2i) -> Array[Vector2i]:
		var points: Array[Vector2i] = []
		if point.x > 0:
			points.push_back(Vector2i(point.x - 1, point.y))
		if point.y > 0:
			points.push_back(Vector2i(point.x, point.y - 1))
		if point.x < (width - 1):
			points.push_back(Vector2i(point.x + 1, point.y))
		if point.y < (height -  1):
			points.push_back(Vector2i(point.x, point.y + 1))
		return points
		
	func next_search() -> int:
		var next_search_distance = 0
		for distance_remaining in search_points.keys():
			var point_list = search_points[distance_remaining]
			if distance_remaining > next_search_distance and point_list.size() > 0:
				next_search_distance = distance_remaining
		return next_search_distance
		
	func search(starting_point: Vector2i, distance: int): # -> Dictionary[Vector2i, Point]
		for i in range(distance-1, 0, -1):
			search_points[i] = []
		var initial_path: Array[Vector2i] = []
		expand_point(starting_point, Path.new(initial_path), distance)
		var search_distance = next_search()
		while search_distance > 0:
			var points_to_search = search_points[search_distance]
			while not points_to_search.size() == 0:
				var point = points_to_search.pop_front()
				expand_point(point, destinations[point], search_distance)
			search_distance = next_search()
		return destinations
		
	func is_tile_passable(point: Vector2i):
		return map_data[point.x][point.y].is_passable()
		
	func expand_point(point: Vector2i, path_to_point: Path, distance: int):
		for neighbour in get_neighbouring_tiles(point):
			if neighbour in destinations:
				pass
			var neighbour_tile = map_data[neighbour.x][neighbour.y]
			var cost = 1
			if not self.uniform_costs:
				cost = neighbour_tile.movement_cost()
			if cost <= distance and (!consider_passability or neighbour_tile.is_passable()):
				var path_to_neighbour = path_to_point.extend(neighbour)
				destinations[neighbour] = path_to_neighbour
				if (distance - cost) > 0:
					search_points[distance - cost].push_back(neighbour)

func _init(map_data):
	self.map_data = map_data


func find_routes(starting_point: Vector2i, movement_distance: int, consider_passability: bool, uniform_costs: bool):
	return Search.new(map_data, consider_passability, uniform_costs).search(starting_point, movement_distance)
