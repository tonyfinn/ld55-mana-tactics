extends Control

class_name ActionButton

signal action_clicked(name: String)
signal summon_clicked(unit_type_name: String)

@export var action_name: String

static var BUTTON_LABELS = {
	Constants.ACTION_MOVE: 'Move',
	Constants.ACTION_SUMMON: 'Summon',
	Constants.ACTION_ATTACK: 'Attack'
}

var label: String:
	get:
		return $ButtonLabel.text
	set(value):
		$ButtonLabel.text = value

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_button_pressed():
	action_clicked.emit(self.action_name)

func _get_minimum_size():
	return Vector2(64, 64)

func set_action(action_name: String):
	$ActionSprite.animation = action_name
	self.action_name = action_name
	$ButtonLabel.text = BUTTON_LABELS[action_name]
