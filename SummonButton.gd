extends Control

class_name SummonButton

signal summon_clicked(unit_type_name: String)

var unit_type_name: String = ""
var disabled: bool = false

func set_unit_type(unit_type_name: String):
	self.unit_type_name = unit_type_name
	$UnitSprite.animation = Constants.UNIT_STATS[unit_type_name][Constants.UATTR_ANIMATION]
	var cost_icon = Constants.UNIT_STATS[unit_type_name][Constants.UATTR_COSTS][0]
	$ResourceSprite.animation = cost_icon
	$ResourceSprite2.animation = cost_icon
	$ResourceSprite3.animation = cost_icon
	
	$DisplayNameLabel.text = Constants.UNIT_STATS[unit_type_name][Constants.UATTR_DISPLAY_NAME]


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_pressed():
	if not disabled:
		summon_clicked.emit(self.unit_type_name)


func _get_minimum_size():
	return Vector2(128, 64)
