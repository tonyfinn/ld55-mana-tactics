extends Node2D
class_name Unit

signal unit_moved(unit: Unit, from_tile: MapTile, to_tile: MapTile)
signal unit_injured(unit: Unit, hp_lost: int, remaining_hp: int)

@export var x: int
@export var y: int
@export var current_tile: MapTile
@export var unit_type: String = ""

var owned_by: Player = null
var has_moved: bool = false
var has_attacked: bool = false
var times_summoned: int = 0

var hp: int = 0

var attack_power: int:
	get:
		return Constants.UNIT_STATS[self.unit_type][Constants.UATTR_ATTACK]
		
var attack_range: int:
	get:
		return Constants.UNIT_STATS[self.unit_type][Constants.UATTR_ATTACK_RANGE]
		
var max_hp: int:
	get:
		return Constants.UNIT_STATS[self.unit_type][Constants.UATTR_HP]
		
var movement_speed: int:
	get:
		return Constants.UNIT_STATS[self.unit_type][Constants.UATTR_MOVEMENT_SPEED]	
			
var unit_animation: String:
	get:
		return Constants.UNIT_STATS[self.unit_type][Constants.UATTR_ANIMATION]
		
var costs:
	get:
		return Constants.UNIT_STATS[self.unit_type][Constants.UATTR_COSTS]
		
var costs_animation: String:
	get:
		return Constants.UNIT_STATS[self.unit_type][Constants.UATTR_COSTS][0]
		
var label: String:
	get:
		return Constants.UNIT_STATS[self.unit_type][Constants.UATTR_DISPLAY_NAME]
	

var grid_pos: Vector2i:
	get:
		return Vector2i(self.x, self.y)
	set(value):
		self.x = value.x
		self.y = value.y
		
func can_summon() -> bool:
	return self.unit_type == Constants.UNIT_TYPE_SUMMONER and times_summoned < 2
	
func can_move() -> bool:
	return not self.has_moved
	
func can_attack() -> bool:
	return self.attack_power > 0 and not has_attacked
	
func reset_for_new_turn():
	self.has_moved = false
	self.has_attacked = false
	self.times_summoned = 0
	$UnitSprite.modulate = Color(1, 1, 1, 1)
	
	
func dim():
	$UnitSprite.modulate = Color(0.3, 0.3, 0.3, 0.8)

# Called when the node enters the scene tree for the first time.
func _ready():
	print($HPBar.size)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	

func internal_move_to(map_tile: MapTile):
	var previous_tile = self.current_tile
	if self.current_tile:
		self.current_tile.current_unit = null
	self.current_tile = map_tile
	map_tile.current_unit = self
	self.grid_pos = map_tile.grid_pos
	self.position = map_tile.screen_pos()
	
func move_to(map_tile: MapTile):
	var previous_tile = self.current_tile
	self.internal_move_to(map_tile)
	unit_moved.emit(self, previous_tile, map_tile)
	self.has_moved = true
	$UnitSprite.modulate = Color(1, 1, 1, 0.5)
	
func coords() -> Vector2i:
	return Vector2i(self.x, self.y)

func set_owned_by(player: Player):
	self.owned_by = player
	$FlagSprite.modulate = player.tint_color
	
func set_type(type: String):
	self.unit_type = type
	self.hp = self.max_hp
	$UnitSprite.animation = self.unit_animation
	
func remove():
	self.current_tile.current_unit = null
	self.queue_free()
	
func take_damage(amount):
	self.hp -= amount
	self.unit_injured.emit(self, amount, self.hp)
	$HPBar.size.x = int((float(self.hp) / float(self.max_hp)) * 40.0)
	
func attack(other_unit: Unit):
	has_attacked = true
	other_unit.take_damage(self.attack_power)
