extends Object
class_name Path

var steps: Array[Vector2i]

func extend(point: Vector2i) -> Path:
	var new_steps = self.steps.duplicate()
	new_steps.push_back(point)
	return Path.new(new_steps)
	
func destination() -> Vector2i:
	return	steps[-1]

func _init(steps_to):
	steps = steps_to
