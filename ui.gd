extends CanvasLayer

signal end_turn
signal action_clicked(action_name: String)
signal summon_clicked(type_name: String)
signal info_clicked
signal game_started
signal game_restarted

@export var action_button_scene: PackedScene
@export var summon_button_scene: PackedScene
@export var popup_text_scene: PackedScene
	
func create_action_button(action_name):
	var action_button = action_button_scene.instantiate()
	action_button.set_action(action_name)
	action_button.connect("action_clicked", _on_action_clicked)
	return action_button
	
func create_summon_button(unit_type):
	var summon_button = summon_button_scene.instantiate()
	summon_button.set_unit_type(unit_type)
	summon_button.connect("summon_clicked", _on_summon_clicked)
	return summon_button
	

func _on_summon_clicked(unit_type_name: String):
	summon_clicked.emit(unit_type_name)

func _on_action_clicked(action_name):
	action_clicked.emit(action_name)

# Called when the node enters the scene tree for the first time.
func _ready():
	$ActionToolbar.hide()
	
func clear_toolbar():
	for existing_action_button in $ActionToolbar.get_children():
		$ActionToolbar.remove_child(existing_action_button)
	
func show_toolbar(action_names: Array[String]):
	clear_toolbar()
	for action_name in action_names:
		var action_button = create_action_button(action_name)
		$ActionToolbar.add_child(action_button)
	$ActionToolbar.show()
	
func show_summon_toolbar(active_unit_types: Array[String], inactive_unit_types: Array[String]):
	clear_toolbar()
	for unit_type in active_unit_types:
		var unit_button = create_summon_button(unit_type)
		print("Adding summon option for " + unit_type)
		$ActionToolbar.add_child(unit_button)
	for unit_type in inactive_unit_types:
		var unit_button = create_summon_button(unit_type)
		unit_button.modulate = Color(0.3, 0.3, 0.3, 1)
		unit_button.disabled = true
		print("Adding summon option for " + unit_type)
		$ActionToolbar.add_child(unit_button)
	$ActionToolbar.show()
	
func show_popup_text(starting_position: Vector2, text: String):
	var popup = popup_text_scene.instantiate()
	popup.position = starting_position
	popup.text = text
	add_child(popup)

func hide_toolbar():
	$ActionToolbar.hide()
	
func set_player(player: Player):
	$TopBarPanel/TopBar/CurrentPlayer.text = "Player " + str(player.player_id + 1)
	$TopBarPanel/TopBar/CurrentPlayerColor.color = player.tint_color
	
func resource_label_text(player: Player, resource: String):
	return (
		str(player.current_resources[resource])
		+ " / " + str(player.max_resources[resource])
		+ " (+" + str(player.resource_income[resource]) + ")"
	)
	
func update_player_resources(player: Player):
	$TopBarPanel/TopBar/FireLabel.text = resource_label_text(player, Constants.RESOURCE_FIRE)
	$TopBarPanel/TopBar/WaterLabel.text = resource_label_text(player, Constants.RESOURCE_WATER)
	$TopBarPanel/TopBar/EarthLabel.text = resource_label_text(player, Constants.RESOURCE_EARTH)
	$TopBarPanel/TopBar/LifeLabel.text = resource_label_text(player, Constants.RESOURCE_LIFE)
	
func set_turn(turn_count: int):
	$TopBarPanel/TopBar/CurrentTurn.text = "Turn " + str(turn_count)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func show_tile_info(tile: MapTile):
	$TileInfoPanel.show_for_tile(tile)
	$TileInfoPanel.show()
	
func show_unit_info(unit: Unit):
	$UnitInfoPanel.show_for_unit(unit)
	$UnitInfoPanel.show()
	
func hide_tile_info():
	$TileInfoPanel.hide()

func hide_unit_info():
	$UnitInfoPanel.hide()

func _on_end_turn_button_pressed():
	end_turn.emit()


func _on_start_game_button_pressed():
	$GameStartContainer.hide()
	$TopBarPanel.show()
	$PermActions.show()
	game_started.emit()
	

func game_over(winner: Player):
	$GameOverContainer/MarginContainer/VBoxContainer/WinnerHeader.text = "Player " + str(winner.player_id + 1) + " Wins!"
	$GameOverContainer.show()
	hide_toolbar()
	$PermActions.hide()


func _on_close_tile_info_button_pressed():
	$TileInfoPanel.hide()


func _on_close_unit_info_button_pressed():
	$UnitInfoPanel.hide()


func _on_info_button_pressed():
	$PermActions/HBoxContainer/InfoButton.text = "Info Mode (Active)"
	info_clicked.emit()
	
func clear_info_mode():
	hide_unit_info()
	hide_tile_info()
	$PermActions/HBoxContainer/InfoButton.text = "Info Mode"


func _on_play_again_button_pressed():
	game_restarted.emit()
	$TopBarPanel.show()
	$PermActions.show()
	$GameOverContainer.hide()
