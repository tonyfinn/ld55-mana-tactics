extends PanelContainer


func show_for_tile(tile: MapTile):
	$MarginContainer/VBoxContainer/TileNameLabel.text = Constants.TILE_NAMES[tile.terrain_type]
	$MarginContainer/VBoxContainer/GridContainer/MovementCostAmount.text = str(tile.movement_cost())
	$MarginContainer/VBoxContainer/GridContainer/PositionValue.text = "(" + str(tile.grid_pos.x + 1) + ", " + str(tile.grid_pos.y + 1) + ")"
	$MarginContainer/VBoxContainer/GridContainer/ResourceAmount.text = find_resource_text(tile)
	var owned_text = "None"
	if tile.owned_by != null:
		owned_text = "Player " + str(tile.owned_by.player_id + 1)
	$MarginContainer/VBoxContainer/GridContainer/OwnerValue.text = owned_text
	var preview_tile = $MarginContainer/VBoxContainer/PanelContainer/Spacer/MapTile
	preview_tile.set_owned_by(tile.owned_by)
	preview_tile.set_type(tile.terrain_type)
	
	
func find_resource_text(tile: MapTile) -> String:
	var count_by_resource = {}
	for resource in tile.output_resources():
		if resource not in count_by_resource:
			count_by_resource[resource] = 1
		else:
			count_by_resource[resource] += 1
	for key in count_by_resource:
		return Constants.RESOURCE_NAMES[key] + " (" + str(count_by_resource[key]) + ")"
	return 'None'

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
