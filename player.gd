extends Object
class_name Player

var owned_tiles: Array[MapTile] = []

var player_id: int
var tint_color: Color
var units: Array[Unit]

var current_resources = {
	Constants.RESOURCE_FIRE: 0,
	Constants.RESOURCE_WATER: 0,
	Constants.RESOURCE_EARTH: 0,
	Constants.RESOURCE_LIFE: 0
}

var max_resources:
	get:
		var resources = {}
		for resource in Constants.ALL_RESOURCES:
			resources[resource] = 2
		for tile in self.owned_tiles:
			for resource in tile.output_resources():
				resources[resource] += 2
		print(resources)
		return resources
		
var resource_income:
	get:
		var resources = {}
		for resource in Constants.ALL_RESOURCES:
			resources[resource] = 0
		for tile in self.owned_tiles:
			for resource in tile.output_resources():
				resources[resource] += 1
		return resources

func capture_tile(tile: MapTile):
	tile.set_owned_by(self)
	if tile not in self.owned_tiles:
		self.owned_tiles.push_back(tile)
		
func start_turn():
	for unit in units:
		unit.reset_for_new_turn()

	var caps = self.max_resources
	var income = self.resource_income

	for tile in self.owned_tiles:
		for resource in income:
			current_resources[resource] = min(caps[resource], current_resources[resource] + income[resource])

func end_turn():
	pass
	
func deduct_costs(costs):
	for cost in costs:
		current_resources[cost] -= 1

func can_afford(costs) -> bool:
	var costs_by_type = {
		Constants.RESOURCE_FIRE: 0,
		Constants.RESOURCE_WATER: 0,
		Constants.RESOURCE_EARTH: 0,
		Constants.RESOURCE_LIFE: 0
	}
	for cost in costs:
		costs_by_type[cost] += 1
		
	for cost in costs_by_type.keys():
		if costs_by_type[cost] > current_resources[cost]:
			return false
	return true

func _init(player_id: int):
	self.player_id = player_id
	if player_id == 0:
		tint_color = Color(1.4, 0.6, 0.6, 1)
	else:
		tint_color = Color(0.6, 0.5, 1.8, 1)
	
	for resource in Constants.ALL_RESOURCES:
		current_resources[resource] = 2
