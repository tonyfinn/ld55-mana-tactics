extends Node2D
class_name PopupText

func _ready():
	$Timer.start()


var text: String:
	get:
		return $PopupTextMessage.text
	set(value):
		$PopupTextMessage.text = value

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.position.y -= (delta * 50)

func _on_timer_timeout():
	print("Message expired: ", self.text)
	self.queue_free()
